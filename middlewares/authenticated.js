'use strict'
var jwt=require('jwt-simple');
var moment=require('moment');
var secret='clave_secreta_curso';
// El middleware va a actuar antes de que se haga la peticion
exports.ensureAuth= function(req,res,next){
    if(!req.headers.authorization){
        return res.status(403).send({message:'La peticion no tiene cabecera de authenticacion'})
    }

    var token =req.headers.authorization.replace('Bearer ','');
    try{
            var payload=jwt.decode(token,secret);
            // Esto es para decodificar
            if(payload.exp<=moment().unix){
                return res.status(401).send({message:'Token ha expirado'});

            }
    }catch(ex){
        // console.log(ex);
        return res.status(404).send({message:'Token no valido'});
    }
    req.user = payload;
    next();
}