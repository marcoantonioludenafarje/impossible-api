'use strict'
var Artist=require('../models/artist');
var Album=require('../models/album');
var Song=require('../models/song');
var bcrypt=require('bcrypt-nodejs');
var jwt=require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
// Estos sirven para trabajar ficheros y poner devolver l imagen 
var fs=require('fs');
var path=require('path');
function getArtist(req,res){
    var artistId=req.params.id;
    Artist.findById(artistId,(err,artist)=>{
        if(err){
            res.status(500).send({message:'Error al buscar al artista'});
        }else{
            if(!artist){
                res.status(404).send({message:'El artista no existe'});
            }else{
                res.status(200).send({Artist:artist});
            }

        }



    });
}

function saveArtist(req,res){
    var artist= new Artist();
    var params=req.body;
    artist.name=params.name;
    artist.description=params.description;
    artist.image='null';
    artist.save((err,artistStored)=>{
        if(err){
            res.status(500).send({message:'Error al guardar al artista'});
        }else{
            if(!artistStored){
                res.status(404).send({message:'No se ha registrado al artista'});
            }else{
                res.status(200).send({artist:artistStored});
            }
        }


    });


}

function getArtists(req,res){
    if(req.params.page){
        var page= req.params.page;
    }else{
        var page=1;
    }

var itemsPerPage=4;
    Artist.find().sort('name').paginate(page,itemsPerPage,function(err,artists,total){
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!artists){
            res.status(404).send({message:'No hay artistas'});
            }else{
            return res.status(200).send({
                total_items:total,
                artists:artists
            });
            }
        }


    });

}

function updateArtist(req,res){
var artistId=req.params.id;
var update=req.body;
Artist.findByIdAndUpdate(artistId,update,(err,artistUpdated)=>{
if(err){
    res.status(500).send({message:'Error interno'});

}else{
        if(!artistUpdated){
            res.status(404).send({message:'Error en el servidor'});
        }else{
            res.status(200).send({message:'El artista que se ha actualizado es el siguiente',artist:artistUpdated});
        }
}
});
}
function deleteArtist(req,res){
    // vamos a eliminar al artista con sus albyumes y todas las canciones 
    var artistId=req.params.id;
    Artist.findByIdAndRemove(artistId,(err,artistRemoved)=>{
    if(err){
        res.status(500).send({message:'Error al eliminar artista'});
    }else{
        if(!artistRemoved){
            res.status(404).send({message:'El artista no ha sido eliminado'});
        }else{
          Album.find({artist:artistRemoved._id}).remove((err,albumRemoved)=>{
          
            if(err){
                    res.status(500).send({message:'Error al eliminar album'});
                }else{
                    if(!albumRemoved){
                        res.status(404).send({message:'No se ha podido eliminar el albun del artista'});
                    }else{
                        Song.find({album:albumRemoved._id}).remove((err,songRemoved)=>{

                        if(err){
                                res.status(500).send({message:'Error al eliminar cancion'});
                            }else{
                                if(!songRemoved){
                                    res.status(404).send({message:'No se ha eliminado el'});
                                }else{
                                    res.status(200).send({artistRemovido:artistRemoved,AlbumRemovido:albumRemoved,songRemovida:songRemoved})
                                }
                            }









                        });
                    }
                }






          });
        }
    }


    });

}
function uploadImage(req,res){
    var artistId=req.params.id;
    var file_name= 'No subido ....'

   if(req.files){
        console.log("Todo deberia ir bien ");
        var file_path= req.files.image.path;
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var ext_split=file_name.split('\.');
        var file_ext=ext_split[1];
        // 'uploads\\users\\ydnOizWdGlxDOM9RT3CwG9jY.png' 
        console.log(ext_split);
        if(file_ext=='png' || file_ext=='jpg' || file_ext=='gif'){
            Artist.findByIdAndUpdate(artistId,{image:file_name},(err,ArtistUpdated)=>{
                if(!ArtistUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                console.log("Todo debe ir bien");
                res.status(200).send({artist:ArtistUpdated});
            }
            });
        }else{
             res.status(200).send({message:'Extension del archivo no valida'});
        }
    }else{
        console.log(req.files);
        console.log("Holi");
        res.status(200).send({message:'No has subido ninguna imagen ...'});

    }


}


function getImageFile(req,res){
    var imageFile=req.params.imageFile;
    var path_file='./uploads/artists/'+imageFile;
    fs.exists(path_file ,function(exists){
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message:'No existe la imagen ...'})
        }
    });
}

module.exports={
getArtist,
saveArtist,
getArtists,
updateArtist,
deleteArtist,
uploadImage,
getImageFile
};