'use strict'
var User=require('../models/user');
var Rol=require('../models/rol');
var Reto=require('../models/reto');
var Nivel=require('../models/nivel');
var Permiso=require('../models/permiso');
var UserFacebook=require('../models/userFacebook');
var bcrypt=require('bcrypt-nodejs');
var jwt=require('../services/jwt');
var global=require('../variables/nivel.js');
var genCod=require('../services/genCod');
var fs=require('fs');
var path=require('path');
var jwts=require('jwt-simple');

function pruebas(req,res){
    res.status(200).send({
        message:'Probando una accion del controlador de usuario del api rest Node js Y Mongo'
    })

}
function getGlobales(){

    global.variables.set("ROLES","cositas");
        Rol.find({},function(err,roles){
            if(err){
                global.variables.set("error","Error en obtener los permisos");
            }else{
                global.variables.set("roles",roles);
                Reto.find({},function(err, retos){
                    if(err){
                        console.log("hubo un error");
                        global.variables.set("error","Error en obtener los retos");
                    }else{
                        global.variables.set("retos",retos);
                        console.log("Todo anda bien ");
                    Nivel.find({},function(err, niveles){
                            if(err){
                                console.log("hubo un error");
                                global.variables.set("error","Error en obtener los niveles");
                            }else{
                                global.variables.set("niveles",niveles);
                                console.log("Todo anda bien en niveles ");
                            }

                        });
                    }

                });
            }
        });


}
function saveUser(req,res){
    console.log(req.body);
    var user=new User();
    var params=req.body;
    var rol_name;
    if(params.personalData.rol=="ALUMNO"){
    rol_name="ROL_ALUM";
    }else if(params.personalData.rol=="PROFESOR"){
    console.log("Es profesor");
    rol_name="ROL_PROF";
    }else{
    rol_name="ROL_ALUM";
    }
    user.personalData.name=params.personalData.name;
    user.personalData.lastName=params.personalData.lastName;
    user.accountData.email=params.accountData.email;
    user.accountData.cargo="exp";
    user.accountData.nivel=1;
    user.accountData.exp=0;
    user.accountData.tCoins=0;
    try {
        var niveles=global.variables.get("niveles");
        var nivel1 = niveles.filter(( obj )=>{
            return obj.num_nivel == 1;
        });   
        var roles=global.variables.get("roles");
        var resultado = roles.filter(( obj )=>{
            return obj.name == rol_name;
        });       
    } catch (error) {
        res.status(404).send({message:"Te estas pasando de verga"});
    }



        User.find({"accountData.email":user.accountData.email},(err, user_repetidos)=>{
            if(err){
                res.status(500).send({message:'Error al comprobar el usuario'});
            }else{
                if(user_repetidos.length>0){
                    console.log("veamos cuantos users nos manda");
                    console.log(user_repetidos);
                    res.status(200).send({message:"El usuario ya existe"});
                }else{
                    
                if(params.accountData.password && resultado[0].name && nivel1[0].num_nivel){
                    user.personalData.rol=resultado[0].name;
                    user.accountData.exp_req=nivel1[0].cantExpReq;

                        bcrypt.hash(params.accountData.password,null,null,function(err,hash){
                            user.accountData.password=hash;
                            if(user.personalData.name !=null && user.personalData.lastName !=null && user.accountData.email !=null && user.personalData.rol !=null){
                                user.save((err,userStored)=>{
                                    if(err){
                                        res.status(500).send({message:'Error alguardar el usuario'});
                                    }else{
                                        if(!userStored){
                                            res.status(404).send({message:'No se ha registrado el usuario'});
                                        }else{
                                            res.status(200).send({usuario:userStored});
                                        }
                                    }
                                });

                            }else{
                                res.status(200).send({message:'Rellena todos los campos'});
                            }

                        });

                }else{
                        res.status(200).send({message:'Introduce la contraseña'});
                    }
    


                }
            }

          });
        


    
    
    

}
function createAvatar(req,res){
    var userId=req.params.id;
    var update=req.body;
    console.log(update);
    User.findByIdAndUpdate(userId,{$set : { 'avatar': update }},(err,userUpdated)=>{
    // User.findByIdAndUpdate(userId,{$set : { 'personalData.name': 'CambioData' }},(err,userUpdated)=>{
        
        if(err){
            res.status(500).send({message:'Error al actualizar el usuario'});
        }else{
            if(!userUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                res.status(200).send({userUpdated:userUpdated});
            }
        }

    });

}
function guardarAvance(req,res){

      var userId=req.params.id;
      var num_reto=req.params.num_reto;
      var token =req.headers.authorization.replace('Bearer ','');
      var secret='clave_secreta_curso';
      var payload=jwts.decode(token,secret);

      if(payload.id==userId){
        User.findOne({_id:userId},'accountData.exp accountData.nivel accountData.tCoins',(err,user)=>{
            if(err){
            res.status(500).send({message:'Error alguardar el usuario'});
            }else{
                if(!user){
                    res.status(404).send({message:"El usuario no se encuentra"});
                }else{
                    var act_coins=user.accountData.tCoins;
                    var act_exp=user.accountData.exp;
                    var act_nivel=user.accountData.nivel;

                    var retos=global.variables.get("retos");
                    var niveles=global.variables.get("niveles");
                    console.log("Los niveles");
                    console.log(niveles);
                    console.log(act_nivel);
                    

                    try{
                        var reto = retos.filter(( obj )=>{
                            return obj.num_reto == num_reto;
                        }); //Esto nos devuelve exp: 200  num_reto:1 tCoins:100
                        var nivel = niveles.filter(( obj )=>{
                            return obj.num_nivel == act_nivel;
                        });
                        var niv_siguiente=niveles.filter(( obj )=>{
                            return obj.num_nivel == act_nivel+1;
                        });
                        console.log("vendra algo");
                        console.log(reto);
                        console.log(nivel);
                        console.log(niv_siguiente);
                        var nuevo_nivel;
                        var nueva_exp;
                        var nuevo_coi;
                        var exp_req;
                        if(act_exp+reto[0].exp>nivel[0].cantExpReq){
                            //ACA ES CUANDO VA A UN NUEVO NIVEL
                                nuevo_nivel=nivel[0].num_nivel+1;
                                nueva_exp=act_exp+reto[0].exp-nivel[0].cantExpReq;
                                nuevo_coi=act_coins+reto[0].tCoins;
                                exp_req=niv_siguiente[0].cantExpReq;                        
                        }else{
                            //ACA ES CUANDO ES EN EL MISMO NIVEL 
                                nuevo_nivel=nivel[0].num_nivel;
                                nueva_exp=act_exp+reto[0].exp;
                                nuevo_coi=act_coins+reto[0].tCoins;
                                exp_req=nivel[0].cantExpReq;
                        }
                        User.findByIdAndUpdate(userId,{$set : { 'accountData.exp': nueva_exp,'accountData.tCoins': nuevo_coi,'accountData.nivel': nuevo_nivel,'accountData.exp_req': exp_req  }},(err,userUpdated)=>{
                        if(err){
                            console.log(err);
                            res.status(500).send({message:'Error al actualizar el usuario'});
                        }else{
                            if(!userUpdated){
                                res.status(404).send({message:'No se ha podido actualizar el usuario'});                                
                            }else{
                                res.status(200).send({exp:nueva_exp,tCoins:nuevo_coi,nivel:nuevo_nivel,exp_req:exp_req});
                            }
                        }
                    });    
                       
                    }catch(erro_sol){
                        res.status(200).send("pequeño pendejo que tratas de poner leveles que no son permitidos");
                    }
                    // if(act_exp+reto[0].exp>nivel[0].cantExpReq){
                    //     nuevo_nivel=nivel[0].num_nivel+1;
                    //     nueva_exp=act_exp+reto[0].exp-nivel[0].cantExpReq;
                    //     nuevo_coi=act_coins+reto[0].tCoins;                        
                    // }else{
                    //     nuevo_nivel=nivel[0].num_nivel;
                    //     nueva_exp=act_exp+reto[0].exp;
                    //     nuevo_coi=act_coins+reto[0].tCoins;
                    // }

                    
            }
        }



        })

    //       console.log("Entro aqui EN EL REQUEST");
    //     var userCoins=req.body.tCoins;
    //     var totalCoins=result.tCoins+userCoins;
    //     var userExp=req.body.exp;
    //     var totalExp=result.exp+userExp;

    //     var update={
    //         'accountData.exp':totalExp,
    //         'accountData.tCoins':totalCoins
    //     }
    //     var globales=global.variables.get("permisos");
    //     res.status(200).send({update:update});
    //   }else{
    //     console.log("llego al error");
    //     res.status(400).send({message:'No hacer alteraciones o habra suspención de la cuenta '});
    //   }

    }
}
// rolRegister
function rolRegister(req,res){
    var rol=new Rol();
    var params=req.body;
    rol.name=params.name;
    rol.description=params.description;


    rol.save((err,rolStore)=>{
        if(err){
            res.status(500).send({message:'Error alguardar el usuario'});
        }else{
            if(!rolStore){
                res.status(404).send({message:'No se ha registrado el usuario'});
            }else{
            console.log("Aca viene lo bonito");
            console.log(rolStore);
            res.status(200).send({rol:rolStore});
            }
            }
        });
}

function getRols(req,res){
    if(req.params.cant){
        var cant= req.params.cant;
    }else{
        var cant=1;
    }
    //   res.status(200).send({message:'Si llega'});
    var itemsPerPage=20;
    Rol.find().sort('name').paginate(1,itemsPerPage,function(err,roles,total){
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!roles){
            res.status(404).send({message:'No hay artistas'});
            }else{
                console.log("La cantidad total son");
                console.log(total);
            return res.status(200).send({
                total_items:total,
                roles:roles
            });
            }
        }


    });

}


function rolUpdate(req,res){
    var rolId=req.params.id;
    var update=req.body;
    // if(rolId !=req.user.sub){
    //    return  res.status(500).send({message:'No tienes permiso para actualizar este usuario'});
    // }
    Rol.findByIdAndUpdate(rolId,update,(err,rolUpdated)=>{
        if(err){
            res.status(500).send({message:'Error al actualizar el usuario'});
        }else{
            if(!rolUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                getGlobales();
                res.status(200).send({rol:rolUpdated});
            }
        }

    })

}


// rolRegister
function permisoRegister(req,res){
    var permiso=new Permiso();
    var params=req.body;
    permiso.ruta=params.ruta;
    permiso.save((err,permisoStore)=>{
        if(err){
            res.status(500).send({message:'Error alguardar el usuario'});
        }else{
            if(!permisoStore){
                res.status(404).send({message:'No se ha registrado el usuario'});
            }else{
            res.status(200).send({permiso:permisoStore});
            }
            }
        });
}




function getPermisos(req,res){
    if(req.params.cant){
        var cant= req.params.cant;
    }else{

        var cant=1;
    }
    //   res.status(200).send({message:'Si llega'});
    var itemsPerPage=20;
    Permiso.find().sort('ruta').paginate(1,itemsPerPage,function(err,permisos,total){
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!permisos){
            res.status(404).send({message:'No hay artistas'});
            }else{
            return res.status(200).send({
                total_items:total,
                permisos:permisos
            });
            }
        }


    });

}



function permisosDeleted(req,res){
console.log(req.body);
// res.status(200).send({message:'Parece que salio todod bien'});
if(req.body.length>0){
    var okey=true;
    for(var i=0;i<req.body.length;i++){
            Permiso.findByIdAndRemove(req.body[i],(err,songRemoved)=>{

                                    if(err){
                                            // problemas.push(req.body[i]);
                                            okey=false;
                                        }else{
                                            if(!songRemoved){
                                                // problemas.push(req.body[i]);
                                                okey=false;
                                            }else{
                                                // res.status(200).send({songRemovida:songRemoved})
                                                okey=true;
                                            }
                                        }


            });
    }

    if(okey){
        res.status(200).send({message:"Se elimino todo correctamente",estaBien :okey});
    }else{
         res.status(404).send({message:"Ocurrio un problema",estaBien :okey});
    }


}else{
res.status(200).send({message:'Lo mandado no es valido'});    
}

}

function permisosDeleted1(req,res){
console.log(req.body);
// res.status(200).send({message:'Parece que salio todod bien'});
if(req.body.length>0){
    var contador=0;
    var problemas=[];
console.log("El primer elemento es" );
console.log(req.body[0]);
            Permiso.findByIdAndRemove(req.body[0],(err,songRemoved)=>{

                                    if(err){
                                            // problemas.push(req.body[i]);
                                            res.status(200).send({message:"error no se encuentra"});
                                        }else{
                                            if(!songRemoved){
                                                // problemas.push(req.body[i]);
                                                res.status(200).send({message:"error no se encuentra"});
                                            }else{
                                                // res.status(200).send({songRemovida:songRemoved})
                                                // res.status(200)
                                                // contador++;
                                                 res.status(200).send({message:"Se elimino correctamente"});
                                            }
                                        }


            });
 




}else{
res.status(200).send({message:'Lo mandado no es valido'});    
}

}





function saveUser1(req,res){
var user= new User();
var params=req.body;
user.name=params.name;
user.surname=params.surname;
user.email=params.email;
user.image='null';
user.atc=params.atc;
user.parametros.verificarCodAcceso=false;
console.log("Params body");
console.log(params);
console.log("Params  params");
console.log(req.params);
if(params.sexo=="mujer"){
user.sexo=0;
}else{
user.sexo=1;
} 
//Si es estudiante se envia parametro tipoUsuario=0
//Si es profesor se envia parametro tipoUsuario=1
//Si es colegio se envia parametro tipoUsuario=2
var tipoUsuario= req.params.tipoUsuario;
if(tipoUsuario==0){
user.role='ROLE_EST';
user.colegio=params.colegio;
user.codigoColegio=params.codigoColegio;
user.gradoEstudio=params.gradoEstudio;
}else if(tipoUsuario==1){
user.role='ROLE_MENTOR';

}else if(tipoUsuario==2){
    user.role="ROLE_COLE";
    user.ruc=params.ruc; 
}
if(params.password){
    //encriptar contraseña y guardar datos
    bcrypt.hash(params.password,null,null,function(err,hash){
        user.password=hash;
        if(user.name !=null && user.surname !=null && user.email !=null){
            //guarde el usuario en la base de datos
            user.save((err,userStored)=>{
                if(err){
                    res.stteratus(500).send({message:'Error alguardar el usuario'});
                }else{
                    if(!userStored){
                        res.status(404).send({message:'No se ha registrado el usuario'});
                    }else{
                        console.log("Aca viene lo bonito");
                        console.log(userStored);
                        res.status(200).send({user:userStored});
                    }
                }
            });

        }else{
            res.status(200).send({message:'Rellena todos los campos'});
        }

    });

}else{
    res.status(200).send({message:'Introduce la contraseña'});
}

}

function sendMessage(req,res){
var user= new User();
var params=req.body;
user.name=params.name;
user.surname=params.surname;
user.email=params.email;
user.image='null';
user.atc=params.atc;
user.parametros.verificarCodAcceso=false;
console.log("Params body");
console.log(params);
console.log("Params  params");
console.log(req.params);
if(params.sexo=="mujer"){
user.sexo=0;
}else{
user.sexo=1;
} 
//Si es estudiante se envia parametro tipoUsuario=0
//Si es profesor se envia parametro tipoUsuario=1
//Si es colegio se envia parametro tipoUsuario=2
var tipoUsuario= req.params.tipoUsuario;
if(tipoUsuario==0){
user.role='ROLE_EST';
user.colegio=params.colegio;
user.codigoColegio=params.codigoColegio;
user.gradoEstudio=params.gradoEstudio;
}else if(tipoUsuario==1){
user.role='ROLE_MENTOR';

}else if(tipoUsuario==2){
    user.role="ROLE_COLE";
    user.ruc=params.ruc; 
}
if(params.password){
    //encriptar contraseña y guardar datos
    bcrypt.hash(params.password,null,null,function(err,hash){
        user.password=hash;
        if(user.name !=null && user.surname !=null && user.email !=null){
            //guarde el usuario en la base de datos
            user.save((err,userStored)=>{
                if(err){
                    res.status(500).send({message:'Error alguardar el usuario'});
                }else{
                    if(!userStored){
                        res.status(404).send({message:'No se ha registrado el usuario'});
                    }else{
                        console.log("Aca viene lo bonito");
                        console.log(userStored);
                        res.status(200).send({user:userStored});
                    }
                }
            });

        }else{
            res.status(200).send({message:'Rellena todos los campos'});
        }

    });

}else{
    res.status(200).send({message:'Introduce la contraseña'});
}

}


function loginUser(req,res){
    console.log("estamos en loginUser");
    console.log(req.body);
    var params=req.body;
    var email=params.email;
    console.log(email);
    var password=params.password;
    


    User.findOne({'accountData.email':email},(err,user)=>{
        if(err){
            console.log("Veamos 1");
            res.status(500).send({message:'Hubo un error en la peticion'});

        }else{
            console.log("Veamos que traes");
            console.log(user);
            if(!user){
                console.log("veamos 2");
                res.status(404).send({message:'El usuario no existe'});
            }else{
                console.log(user);
            var rol_name=user.personalData.rol;
            var roles=global.variables.get("roles");
            var resultado = roles.filter(( obj )=>{
                return obj.name == rol_name;
            });
            

            bcrypt.compare(password,user.accountData.password,function(err,check){
                    if(check){
                        if(resultado.length==1){
                            user.personalData.rol=resultado[0];
                            var token=jwt.createToken(user);
                            var rolId=user.personalData.rol;
                            res.status(200).send({identity:user,token:token});     
                        }else{
                            console.log(resultado);
                            res.status(200).send({message:"Su cuenta tiene un comportamiento extraño , mande un mensaje en la pagina principal"});
                        }
                    }else{
                        console.log("LLego aca de forma incorrecta");
                            res.status(404).send({message:'El usuario no ha podido logearse'});    
                    }
                });
            }
        }

    }
    );

}
function loginUserFacebook(req,res){
    // console.log("Entro al servicio backend");
    // console.log(req.body);
    var userFacebook= new UserFacebook();
    var params=req.body;
    var codigo=params.datosBasicos.id;
    userFacebook.codigo=params.datosBasicos.id;
    userFacebook.idFacebook=params.datosBasicos.id;
    userFacebook.nombres=params.datosBasicos.first_name;
    userFacebook.apellidos=params.datosBasicos.last_name;
    userFacebook.correo=params.datosBasicos.email;
    userFacebook.direccion.direccionExacta=params.datosBasicos.location.name;
    userFacebook.fechaNacimiento=params.datosBasicos.birthday;
    // userFacebook.fechaCreacionCuenta=Date.getDate();
    userFacebook.role='ROLE_USER';
    // var params=req.body;
    // var email=params.email;
    // var password=params.password;

    UserFacebook.findOne({codigo:codigo.toLowerCase()},(err,user)=>{
        // console.log("Aca encuentro el user a ver si hay uno");
        // console.log(user);
        if(err){
            res.status(500).send({message:'Hubo un error en la peticion'});

        }else{
            if(!user){
                console.log("Veremos lo que hay en userFacebook");
                console.log(userFacebook);
                if(userFacebook.idFacebook!=null && userFacebook.nombres !=null  && userFacebook.correo !=null){
                //guarde el usuario en la base de datos
                console.log("Si va a guardar con fe ");
                        userFacebook.save((err,userStored)=>{
                            if(err){
                                res.status(500).send({message:'Error alguardar el usuario'});
                            }else{
                                if(!userStored){
                                    res.status(404).send({message:'No se ha registrado el usuario'});
                                }else{
                                    res.status(200).send({userFacebook:userStored});
                                }
                            }
                        });
                }else{
                        res.status(200).send({message:'No tienen los datos basicos los campos'});
                }
            }else{
                //si hay usuario entonces 

                        //devolver los datos del usuario logeado
                        console.log("Existe gethash??");
                        console.log(params.gethash);
                        if(params.gethash){
                            console.log("Se esta enviando el token");
                            res.status(200).send({
                                token:jwt.createToken(user),userFacebook:user
                            });
                            
                        }else{
                            res.status(200).send({user:user});
                        }
            }
        }

    }
);

}






function updateUser(req,res){
    var userId=req.params.id;
    var update=req.body;
    console.log(update);
    if(userId !=req.user.sub){
       return  res.status(500).send({message:'No tienes permiso para actualizar este usuario'});
    }
    User.findByIdAndUpdate(userId,update,(err,userUpdated)=>{
        if(err){
            res.status(500).send({message:'Error al actualizar el usuario'});
        }else{
            if(!userUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                res.status(200).send({user:userUpdated});
            }
        }

    })

}
function uploadImage(req,res){
    var userId=req.params.id;
    var file_name='No subido ...';

    if(req.files){
        console.log("Todo deberia ir bien ");
        var file_path= req.files.image.path;
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var ext_split=file_name.split('\.');
        var file_ext=ext_split[1];
        // 'uploads\\users\\ydnOizWdGlxDOM9RT3CwG9jY.png' 
        console.log(ext_split);
        if(file_ext=='png' || file_ext=='jpg' || file_ext=='gif'){
            User.findByIdAndUpdate(userId,{image:file_name},(err,userUpdated)=>{

                if(!userUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                console.log("Todo debe ir bien");
                res.status(200).send({image:file_name,user:userUpdated});
            }
            });
        }else{
             res.status(200).send({message:'Extension del archivo no valida'});
        }
    }else{
        console.log(req.files);
        console.log("Holi");
        res.status(200).send({message:'No has subido ninguna imagen ...'});

    }

}

function getImageFile(req,res){
    var imageFile=req.params.imageFile;
    var path_file='./uploads/users/'+imageFile;
    fs.exists(path_file ,function(exists){
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message:'No existe la imagen ...'})
        }

    });

}

function getUsers(req,res){
    if(req.params.page){
        var page= req.params.page;
    }else{
        var page=1;
    }
console.log(page);
var itemsPerPage=3;
    User.find().sort('name').paginate(page,itemsPerPage,function(err,users,total){
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!users){
            res.status(404).send({message:'No hay usuario'});
            }else{
            return res.status(200).send({
                total_items:total,
                users:users
            });
            }
        }


    });

}

function addResult(req,res){
    var update=req.body;
    console.log("Veamos que viene todo");
    console.log(update);
    var suma=0;
    for (var i = 0; i <update.testResultado.puntajeSkills.length; i++) {
    suma=suma+update.testResultado.puntajeSkills[i].puntaje ;
    }
    console.log("La suma es ");
    console.log(suma);
 
    update.testResultado.resultadosPorcentajes=JSON.parse(JSON.stringify(update.testResultado.puntajeSkills));

    for (var i = 0; i <update.testResultado.resultadosPorcentajes.length; i++) {
    update.testResultado.resultadosPorcentajes[i].puntaje=parseFloat(((update.testResultado.resultadosPorcentajes[i].puntaje/suma)*100).toFixed(2))
    }
    update.testResultado.resultadosPorcentajes.sort(function(a,b){
    return b.puntaje-a.puntaje;
    });

    for(var i = 0; i <update.testResultado.puntajeSkills.length; i++) {
    update.testResultado.resultadosPorcentajes[i].puntaje ;
    }
    console.log("yA HEMOS CREADO RESULTADOS pORCENTAJES");
    console.log(update.testResultado);
    User.findByIdAndUpdate(update._id,update,(err,userUpdated)=>{
        if(err){
            res.status(500).send({message:'Error al actualizar los resultados del usuario'});
        }else{
            if(!userUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                res.status(200).send({user:userUpdated, testResultado:update.testResultado});
            }
        }
    })
}
function generadorCod(req,res){
var userId=req.params.id;
var claveGenerada=genCod.generadorCod(userId);


            User.findByIdAndUpdate(userId,{codigoColegio:claveGenerada},(err,userUpdated)=>{

                if(!userUpdated){
                res.status(404).send({message:'No se ha podido generar la clave de acceso'});
            }else{
                res.status(200).send({codigoColegio:claveGenerada,user:userUpdated});
            }
            });


}

function agregarCodAlumno(req,res){
var userId=req.params.id;
var claveGenerada=req.body.codiAct;

console.log("Debe haber llegado algo");
console.log(userId);
console.log(claveGenerada);
if(claveGenerada.length==6){
    User.findOne({codigoColegio:claveGenerada},(err,colegio)=>{
        if(err){
            res.status(500).send({message:'Hubo un error en la peticion'});

        }else{
            if(!colegio){
                res.status(404).send({message:'No existe el codigo de activacion'});
            }else{
                console.log("Haber que viene");
                console.log(colegio);
                // res.status(200).send({message:"Se encontro algo", colegio:colegio});
                User.findByIdAndUpdate(userId,{claveAccesoAlumno:claveGenerada,permisosAterceros:{colegio:colegio._id},colegio:colegio.name,credenciales:{colegioClave:claveGenerada},parametros:{verificarCodAcceso:true}},(err,estudiante)=>{
                if(err){
                    res.status(500).send({message:'Error interno , intentelo mas tarde'});
                }else{
                    if(!estudiante){
                        res.status(404).send({message:'No se ha podido registrar el codigo de activacion'});
                    }else{
                        console.log("Aca viene el estudiante");
                        console.log(estudiante);
                        res.status(200).send({estudiante:estudiante,colegio:colegio});
                    }
                }

            })



            }
        }

    }
    
);


}
}
function buscarEstudiantesPorColegio(req,res){

    var ColegioId=req.params.idColegio;
    // var colegio=User.find({_id:ColegioId});
    //    console.log(colegio);
    // if(req.body.role=="ROLE_COLE"){
    // var find=User.find({credenciales:{colegioClave:colegioClave}}).sort('gradoEstudio');
    var page=1;
    var itemsPerPage=50;
    User.findOne({_id:ColegioId},(err,colegio)=>{
        if(err){
            res.status(500).send({message:'Hubo un error en la peticion'});

        }else{
            if(!colegio){
                res.status(404).send({message:'No existe el codigo de activacion'});
            }else{
                    console.log(colegio.codigoColegio);
                    User.find({claveAccesoAlumno:colegio.codigoColegio}).sort('name').paginate(page,itemsPerPage,function(err,alumnos,total){
                        if(err){
                            res.status(500).send({message:'Error en la peticion'});
                        }else{
                            if(!alumnos){
                            res.status(404).send({message:'No hay usuario'});
                            }else{
                            res.status(200).send({
                                total_items:total,
                                alumnos:alumnos
                            });
                            }
                        }


                    });
            
            
            
            // .paginate(page,itemsPerPage,function(err,alumnos,total){
            //     if(err){
            //         res.status(500).send({message:'Error en la peticion'});
            //     }else{
            //         if(!alumnos){
            //         res.status(404).send({message:'No hay alumnos con la clave'});
            //         }else{
            //         return res.status(200).send({
            //             total_items:total,
            //             alumnos:alumnos
            //         });
            //         }
            //     }


            // });



            }
        }

    }
    
);





    // }else{
    //     res.status(400).send({message:"No tienes permisos para entrar aca papu"});
    // }


    // if(!artistId){
    //     // sacar todos los albums de la bd 
    //     var find=Album.find({}).sort('title');
    // }else{
    //     // Sacar los albums de un artista concreto en la bd
    //     var find=Album.find({artist:artistId}).sort('year');

    // }

    // find.populate({path:'artist'}).exec((err,albums)=>{
    //     if(err){
    //         res.status(500).send({message:'Error en la peticion'});
    //     }else{
    //         if(!albums){
    //         res.status(404).send({message:'No hay albums'});
    //         }else{
    //         res.status(200).send({albums})
    //         }
    //     }




    // });






}

module.exports={
    pruebas,
    saveUser,
    loginUser,
    updateUser,
    uploadImage,
    getImageFile,
    getUsers,
    loginUserFacebook,
    addResult,
    generadorCod,
    agregarCodAlumno,
    buscarEstudiantesPorColegio,
    rolRegister,
    getRols,
    permisoRegister,
    getPermisos,
    rolUpdate,
    permisosDeleted,
    createAvatar,
    guardarAvance,
    getGlobales
        
};