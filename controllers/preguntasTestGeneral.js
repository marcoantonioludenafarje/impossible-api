'use strict'
var Artist=require('../models/artist');
var Album=require('../models/album');
var Song=require('../models/song');
var Pregunta=require('../models/preguntasVocacionales');
var bcrypt=require('bcrypt-nodejs');
var jwt=require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
// Estos sirven para trabajar ficheros y poner devolver l imagen 
var fs=require('fs');
var path=require('path');
function getPregunta(req,res){
    var preguntatId=req.params.id;
    Pregunta.findById(preguntatId,(err,pregunta)=>{
        if(err){
            res.status(500).send({message:'Error al buscar al preguntaa'});
        }else{
            if(!pregunta){
                res.status(404).send({message:'La pregunta no existe'});
            }else{
                res.status(200).send({pregunta:pregunta});
            }

        }



    });
}

function savePregunta(req,res){
    var pregunta= new Pregunta();
    var params=req.body;
    pregunta.cuerpoPregunta=params.cuerpoPregunta;
    pregunta.alternativas=params.alternativas;
    pregunta.tipoPregunta=params.tipoPregunta;
    pregunta.criterio=params.criterio;
    if(params.esCarta!=true){
    pregunta.esCarta=false;
    }else{
    pregunta.esCarta=true;
    }
    pregunta.fechaCreacion=new Date();
    pregunta.fechaActualizacion=new Date();
    pregunta.save((err,preguntaStored)=>{
        if(err){
            res.status(500).send({message:'Error al guardar al preguntaa'});
        }else{
            if(!preguntaStored){
                res.status(404).send({message:'No se ha registrado al preguntaa'});
            }else{
                res.status(200).send({pregunta:preguntaStored});
            }
        }
    });
}

function getPreguntas(req,res){
    if(req.params.page){
        var page= req.params.page;
    }else{
        var page=1;
    }

var itemsPerPage=4;
    Pregunta.find().sort({esCarta:-1,fechaActualizacion:-1}).paginate(page,itemsPerPage,function(err,preguntas,total){
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!preguntas){
            res.status(404).send({message:'No hay preguntaas'});
            }else{
            return res.status(200).send({
                total_items:total,
                preguntas:preguntas
            });
            }
        }


    });

}
function getPreguntasTest(req,res){
        var page=1;

var itemsPerPage=20;
    Pregunta.find({esCarta:true}).sort().paginate(page,itemsPerPage,function(err,preguntas,total){
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!preguntas){
            res.status(404).send({message:'No hay preguntaas'});
            }else{
            return res.status(200).send({
                total_items:total,
                preguntas:preguntas
            });
            }
        }


    });

}
function updatePregunta(req,res){
var preguntaId=req.params.id;
var update=req.body;
Pregunta.findByIdAndUpdate(preguntaId,update,(err,preguntaUpdated)=>{
if(err){
    res.status(500).send({message:'Error interno'});

}else{
        if(!preguntaUpdated){
            res.status(404).send({message:'Error en el servidor'});
        }else{
            res.status(200).send({message:'El preguntaa que se ha actualizado es el siguiente',pregunta:preguntaUpdated});
        }
}
});
}
function deletePregunta(req,res){
    // vamos a eliminar al preguntaa con sus albyumes y todas las canciones 
    var preguntaId=req.params.id;
    Pregunta.findByIdAndRemove(preguntaId,(err,preguntaRemoved)=>{
    if(err){
        res.status(500).send({message:'Error al eliminar preguntaa'});
    }else{
        if(!preguntaRemoved){
            res.status(404).send({message:'El preguntaa no ha sido eliminado'});
        }else{
            res.status(200).send({preguntaRemovida:preguntaRemoved})
        }
    }


    });

}


module.exports={
// getArtist,
savePregunta,
getPreguntas,
getPregunta,
updatePregunta,
deletePregunta,
getPreguntasTest
// getArtists,
// updateArtist,
// deleteArtist,
// uploadImage,
// getImageFile
};