'use strict'
var Artist=require('../models/artist');
var Album=require('../models/album');
var Song=require('../models/song');
var bcrypt=require('bcrypt-nodejs');
var jwt=require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
// Estos sirven para trabajar ficheros y poner devolver l imagen 
var fs=require('fs');
var path=require('path');

function getSong(req,res){
    var songId=req.params.id;

Song.findById(songId).populate({path:'album'}).exec((err,song)=>{
if(err){
    res.status(500).send({message:'Error en la peticion'});
}else{
    if(!song){
        res.status(404).send({message:'La cancion no existe !!'});
    }else{
        res.status(200).send({songEncontrada:song});
    }
}


});



}

function saveSong(req,res){
    var song=new Song();
    var params=req.body;
    song.number=params.number;
    song.name=params.name;
    song.duration=params.duration;
    song.file='null';
    song.album=params.album;

    song.save((err,songStored)=>{
        if(err){
            res.status(500).send({message:'Error al guardar la song'});
        }else{
            if(!songStored){
                res.status(404).send({message:'No se ha guardado la song'});
            }else{
                res.status(200).send({song:songStored});
            }
        }


    });
}

function getSongs(req,res){
    var albumId=req.params.albumId;
    if(!albumId){
        var find=Song.find({}).sort('number');
    }else{
        var find=Song.find({album:albumId}).sort('number');
    }

    find.populate({
            path:'album',
            populate:{
                path: 'artist',
                model: 'Artist'
            }
            }).exec((err,songs)=>{
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!songs){
            res.status(404).send({message:'No hay albums'});
            }else{
            res.status(200).send({songs});
            }
        }
    });
}

//Actualizar canciones 
function updateSong(req,res){
    var songId=req.params.id;
    var update=req.body;

    Song.findByIdAndUpdate(songId,update,(err,songUpdated)=>{
        if(err){
            res.status(500).send({message:'Error en el servidor'});
        }else{
            if(!songUpdated){
                console.log(songUpdated);
            res.status(404).send({message:'No se ha modificado la cancion'})
            }else{
            res.status(200).send({song:songUpdated});
            }
        }
    });
                   
}


//Eliminar una cancion
function deleteSong(req,res){
var songId=req.params.id;

Song.findByIdAndRemove(songId,(err,songRemoved)=>{

                        if(err){
                                res.status(500).send({message:'Error al eliminar cancion'});
                            }else{
                                if(!songRemoved){
                                    res.status(404).send({message:'No se ha eliminado el'});
                                }else{
                                    res.status(200).send({songRemovida:songRemoved})
                                }
                            }


});
}


function uploadFile(req,res){
    var songId=req.params.id;
    var file_name= 'No subido ....'

   if(req.files){
        console.log("Todo deberia ir bien ");
        var file_path= req.files.file.path;
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var ext_split=file_name.split('\.');
        var file_ext=ext_split[1];
        // 'uploads\\users\\ydnOizWdGlxDOM9RT3CwG9jY.png' 
        console.log(ext_split);
        if(file_ext=='mp3' || file_ext=='ogg'){
            Song.findByIdAndUpdate(songId,{file:file_name},(err,SongUpdated)=>{
                if(!SongUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                console.log("Todo debe ir bien");
                res.status(200).send({Song:SongUpdated});
            }
            });
        }else{
             res.status(200).send({message:'Extension del archivo no valida'});
        }
    }else{
        res.status(200).send({message:'No has subido el fichero de audio...'});

    }


}

function getSongFile(req,res){
    var imageFile=req.params.songFile;
    var path_file='./uploads/songs/'+imageFile;
    fs.exists(path_file ,function(exists){
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            console.log("QUE CHUCHA");
            res.status(200).send({message:'No existe el fichero de audo ...'})
        }

    });

}



//Subir fichero de audio


module.exports={
getSong,
saveSong,
getSongs,
updateSong,
deleteSong,
getSongFile,
uploadFile
}
