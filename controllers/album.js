'use strict'
var Artist=require('../models/artist');
var Album=require('../models/album');
var Song=require('../models/song');
var bcrypt=require('bcrypt-nodejs');
var jwt=require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
// Estos sirven para trabajar ficheros y poner devolver l imagen 
var fs=require('fs');
var path=require('path');

function getAlbum(req,res){
    var albumId=req.params.id;
    Album.findById(albumId).populate({path:'artist'}).exec((err,album)=>{
        if(err){
            res.status(500).send({message:'Error al buscar al artista'});
        }else{
            if(!album){
            res.status(404).send({message:'Error al buscar al artista'});
            }else{
                res.status(200).send({album});


             
            }
        }



    });


}

function saveAlbum(req,res){
    var album=new Album();
    var params=req.body;
    console.log(params);
    album.title=params.title;
    album.description=params.description;
    album.year=params.year;
    album.image='null';
    album.artist=params.artist;
    album.save((err,albumStored)=>{
        if(err){
            res.status(500).send({message:'Error al guardar el albu,'});
        }else{
            if(!albumStored){
                res.status(404).send({message:'No se ha guardado el album'});
            }else{
                res.status(200).send({album:albumStored});
            }
        }


    });



}
function getAlbums(req,res){
    var artistId=req.params.artist;

    if(!artistId){
        // sacar todos los albums de la bd 
        var find=Album.find({}).sort('title');
    }else{
        // Sacar los albums de un artista concreto en la bd
        var find=Album.find({artist:artistId}).sort('year');

    }
    find.populate({path:'artist'}).exec((err,albums)=>{
        if(err){
            res.status(500).send({message:'Error en la peticion'});
        }else{
            if(!albums){
            res.status(404).send({message:'No hay albums'});
            }else{
            res.status(200).send({albums})
            }
        }




    });

}
function updateAlbum(req,res){
    var albumId=req.params.id;
    var update=req.body;

    Album.findByIdAndUpdate(albumId,update,(err,albumUpdated)=>{
        if(err){
            res.status(500).send({message:'Error en el servidor'});
        }else{
            if(!albumUpdated){
            res.status(404).send({message:'No se ha actualizado el album'})
            }else{
            res.status(200).send({album:albumUpdated});
            }
        }



    });



}

function deleteAlbum(req,res){
var albumId=req.params.id;

Album.findByIdAndRemove(albumId,(err,albumEliminado)=>{
if(err){
res.status(500).send({message:'Error al eliminar album'});

}else{
    if(!albumEliminado){
        res.status(404).send({message:'No se encontro al  artista'});

    }else{
        Song.find({album:albumEliminado._id}).remove((err,songRemoved)=>{

                        if(err){
                                res.status(500).send({message:'Error al eliminar cancion'});
                            }else{
                                if(!songRemoved){
                                    res.status(404).send({message:'No se ha eliminado el'});
                                }else{
                                    res.status(200).send({AlbumRemovido:albumEliminado,songRemovida:songRemoved})
                                }
                            }




        });

    }
}






});



}

function uploadImage(req,res){
    var albumtId=req.params.id;
    var file_name= 'No subido ....'

   if(req.files){
        console.log("Todo deberia ir bien ");
        var file_path= req.files.image.path;
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var ext_split=file_name.split('\.');
        var file_ext=ext_split[1];
        // 'uploads\\users\\ydnOizWdGlxDOM9RT3CwG9jY.png' 
        console.log(ext_split);
        if(file_ext=='png' || file_ext=='jpg' || file_ext=='gif'){
            Album.findByIdAndUpdate(albumtId,{image:file_name},(err,AlbumUpdated)=>{
                if(!AlbumUpdated){
                res.status(404).send({message:'No se ha podido actualizar el usuario'});
            }else{
                console.log("Todo debe ir bien");
                res.status(200).send({album:AlbumUpdated});
            }
            });
        }else{
             res.status(200).send({message:'Extension del archivo no valida'});
        }
    }else{
        res.status(200).send({message:'No has subido ninguna imagen ...'});

    }


}

function getImageFile(req,res){
    var imageFile=req.params.imageFile;
    var path_file='./uploads/albums/'+imageFile;
    fs.exists(path_file ,function(exists){
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message:'No existe la imagen ...'})
        }

    });

}



module.exports={
getAlbum,
saveAlbum,
getAlbums,
updateAlbum,
deleteAlbum,
uploadImage,
getImageFile
}
