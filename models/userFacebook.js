'use strict'

var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var UserFacebookSchema=Schema({
    codigo:String,
    idFacebook:String, //codigoFacebook
  	nombres: String, //Facebook
    apellidos:String, //Facebook
    dispositivos:[{os:String}],
    correo:String,
    lenguajeFacebook:String,
    direccion:{
        pais:String,
        provincia:String,
        distrito:String,
        direccionExacta:String
    },
        colegio:[{
        año:Number,
        nombre_Colegio:Number,
        grado:{
            type:String,
            enum: ['5to', '4to', '3ero']
            }
        }],
        fechaNacimiento:Date,
        fechaCreacionCuenta:Date,
        rangoEdad:{max:Number,min:Number},
        //Hasta aca son 
        role:String,

    telefono:String,
    dni: String,
    
    familiarInteresado:[{
        nombreCompleto:String,
        relacionFamiliar:String,
        telefono:String,
        correoElectronico:String,
        nivelDeRelacion:String,
        cargo:String,
        carrera:String,
        viveContigo:Boolean,
        lugarTrabajo:String,
        }],
    estado:{
            type:String,
            enum: ['Activo','Bloqueado']
            },
    tipoCuenta:{
            type:String,
            enum: ['FREE','PREMIUNSCHOOL','PREMIUN']
            },
    fechaFinBloqueo:Date,

    versionPremiunRegistro:[{
        colegioId:{type:Schema.ObjectId,ref:'Colegio'},
        codigoIngresado:String,
        fechaInicio:Date,
        fechaFin:Date

    }],
   	tipoUsuario: {
	    type: String,
	     enum: ['GOOGLE', 'FACEBOOK', 'LOCAL']
  	},
    // roles:{ type:Schema.ObjectId,ref:'Roles'},
    skillsBasicos:{
        Realista:{
            type:Number,
            min:0,
            max:20,
            default:0
        },
        Investigador:{
            type:Number,
            min:0,
            max:20,
            default:0,
            descripcion:String
        },
        Artista:{
            type:Number,
            min:0,
            max:20,
            default:0
        },
        Social:{
            type:Number,
            min:0,
            max:20,
            default:0
        },
        Emprendedor:{
            type:Number,
            min:0,
            max:20,
            default:0
        },
        Convencional:{
            type:Number,
            min:0,
            max:20,
            default:0
        }
    },
    mis_carreras:{ type:Schema.ObjectId,ref:'MisCarreras'}

});
module.exports=mongoose.model('UserFacebook',UserFacebookSchema);