'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var ColegioSchema=Schema({
    nombre:String,
    direccion:{
        pais:String,
        provincia:String,
        distrito:String,
        direccionExacta:String
    },
    alumnos:[{alumno:{type:Schema.ObjectId, ref:'UserFinal'}}],
    codigoVigente:{
        codigoVigente:String,
        fechaActivacion:Date,
        fechaVencimiento:Date
        }
});
module.exports=mongoose.model('Colegio',ColegioSchema);