'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var MisCarreraSchema=Schema({
    alumno:{
         type:Schema.ObjectId,ref:'UserFinal'
    },
    carreras:[{
        carreraId:{type:Schema.ObjectId,ref:'Carrera'},
        nombreCarrera:String,
        ramas:[{
            ramaCarrera:{type:Schema.ObjectId, ref:'RamaCarrera'},
            nombre:String,
            porcentajeAvance:Number,
            clases:[{
                nombreClase:String,
                estado:{
                    type:String,
                    enum:['visto','completado','nuevo','bloqueado']
                },
                preguntasMostrar:Number,
                preguntas:[{
                    numeroPregunta:Number,
                    numeroAlternativas:Number,
                    estado:{type:String,enum:['completado','nuevo']}
                    }]
            }]

        }],
        fechaInicioExploracion:{type: Date, default: Date.now}
    }],
});
module.exports=mongoose.model('MisCarrera',MisCarreraSchema);



