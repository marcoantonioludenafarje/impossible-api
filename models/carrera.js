'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var CarreraSchema=Schema({
    nombre:String,
    descripcion:String,
    campoAccion:[{
        area:String,
        funciones:[String],
        }],
    perfilprofesional:String,
    skillEspecializado:[{
            nombreSkill:String,
            descripcion:String
        }],
    personajesFamosos:[{
        nombre_Completo:String,
        descripcion:String,
        nombreEmpresa:String
    }],
    ramasCarrera:[{
        ramaCarreraId:{type:Schema.ObjectId, ref:'RamaCarrera'},
        nombreCarrera:String
    }]
});
module.exports=mongoose.model('Carrera',CarreraSchema);