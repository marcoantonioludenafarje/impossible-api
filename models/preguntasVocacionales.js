'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var PreguntasTestGeneralSchema=Schema({
            numeroPregunta:Number,
            cuerpoPregunta:String,
            tipoPregunta:String,
            criterio:String,
            esCarta:Boolean,
            numeroAlternativas:Number,
            fechaCreacion:Date,
            fechaActualizacion:Date,
            alternativas:[{
                ordenAlternativa:Number,
                cuerpoAlternativa:String,
                valorSkills:[{skillBasico:String,puntos:Number}],
                }]
});
module.exports=mongoose.model('PreguntasTestGeneral',PreguntasTestGeneralSchema);