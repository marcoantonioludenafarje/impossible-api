'use strict'
var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var MessageSchema=Schema({
    name:String,
    lastName:String,
    cargo:String,
    email:String,
    numberContact:String,
    message:String
});
module.exports=mongoose.model('Message',MessageSchema);