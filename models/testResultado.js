'use strict'

var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var TestResultadoSchema=Schema({
            puntajeSkills:[{
                nombreSkill:String,
                puntaje:Number,
                num:Number  
            }],
            resultadosPorcentajes:[{
                nombreSkill:String,
                puntaje:Number,
                num:Number
            }]

});
module.exports=mongoose.model('TestResultado',TestResultadoSchema);
