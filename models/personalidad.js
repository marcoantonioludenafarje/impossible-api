'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var PersonalidadSchema=Schema({
    nombre:String,
    descripcion:String,
    num:Number,
    carreras:[{nombreCarrera:String}],
    color:{primario:String,secundario:String}
});
module.exports=mongoose.model('Personalidad',PersonalidadSchema);

//emprendedor      background: linear-gradient(to right, rgba(142, 36, 170, 0.60), #4527a0);

//social     background: linear-gradient(to right, rgba(240, 98, 146, 0.60), #f4511e);
