'use strict'
var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var UserSchema=Schema({
    personalData:{
        rol:Schema.Types.Mixed,
        name:String,
        lastName:String,
    },
    accountData:{
        email:String,
        password:String,
        cargo:String,
        nivel:Number,
        exp:Number,
        tCoins:Number,
        exp_req:Number
    },
    avatar:{
        num_brazos:Number,
        num_cuerpo:Number,
        num_lentes:Number,
        num_narices:Number,
        num_ojos:Number,
        num_patas:Number,
        num_peinados:Number,
        extas:Boolean,
        extra_items:{
            num_casco:Number
        },
        equipamiento:{
            ojos:[Number],
            brazos:[Number],
            lentes:[Number],
            narices:[Number],
            
        }  
    },
    estadistica:{
        personalidad:{
            intro:Number,
            extro:Number,
            neuro:Number,
            psico:Number
        },
        inteligencias:{
            log_mat:{
                raz:Number,
                cal:Number,
                agr:Number
            },
            esp:{
                perc:Number,
                visu:Number
            },
            mus:{
                sen:Number,
                esc:Number
            },
            cin_cor:{
                des_motr:Number
            },
            nat:{
                sens:Number
            },
            intra:{
                mismo:Number
            },
            inter:{
                inter:Number
            },
            ling:{
                hab:Number,
                leer:Number,
                escr:Number
            }


        }
    },
    avance:[String]

});

module.exports=mongoose.model('User',UserSchema);