'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;
function validator(val){
var RamaCarreraSchema=Schema({
    nombre:String,
    descripcion:String,
    clases:[{
        nombreClase:String,
        objetivoClase:String,
        videoFile:String,
        preguntasMostrar:Number,
        preguntas:[{
            numeroPregunta:Number,
            cuerpoPregunta:String,
            numeroAlternativas:Number,
            alternativas:[{
                cuerpoAlternativa:String,
                suma:[{skillEspecializado:String,puntos:Number}],
                }],
            }]
    }]
});
module.exports=mongoose.model('RamaCarrera',RamaCarreraSchema);