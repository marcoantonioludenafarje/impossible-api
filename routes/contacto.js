'use strict'
var express=require('express');
var ContactController=require('../controllers/contact');
var api=express.Router();
var md_auth=require('../middlewares/authenticated');

var multipart= require('connect-multiparty');
var md_upload=multipart({uploadDir:'./uploads/users'});

// return this._http.post(this.url+'register/'+tipoUsuario,params,{headers:headers})

api.post('/mensajeria/:tipoUsuario',ContactController.sendMessage);


// api.get('/probando-controlador',md_auth.ensureAuth,UserController.pruebas);
// api.post('/register/:tipoUsuario',UserController.saveUser);
// api.post('/login',UserController.loginUser);
// api.put('/update-user/:id',md_auth.ensureAuth,UserController.updateUser);
// api.post('/upload-image-user/:id',[md_auth.ensureAuth,md_upload],UserController.uploadImage);
// api.get('/get-image-user/:imageFile',UserController.getImageFile);
// api.get('/users/:page?',md_auth.ensureAuth,UserController.getUsers);
// api.post('/loginFacebook',UserController.loginUserFacebook);
// api.put('/addResultado',UserController.addResult);
// api.get('/users/genCod/:id',md_auth.ensureAuth,UserController.generadorCod);
// api.post('/agregarCodigo/:id',md_auth.ensureAuth,UserController.agregarCodAlumno);
// api.get('/estudiantesxColegio/:idColegio',md_auth.ensureAuth,UserController.buscarEstudiantesPorColegio);


module.exports=api;
