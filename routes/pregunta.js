'use strict'
var express=require('express');
var ArtistController=require('../controllers/artist');
var PreguntaController=require('../controllers/preguntasTestGeneral');
var api=express.Router();
var md_auth=require('../middlewares/authenticated');
var multipart= require('connect-multiparty');
var md_upload=multipart({uploadDir:'./uploads/artists'});

api.get('/pregunta/:id',md_auth.ensureAuth,PreguntaController.getPregunta);
api.post('/savePregunta',md_auth.ensureAuth,PreguntaController.savePregunta);
api.get('/preguntas/:page?',md_auth.ensureAuth,PreguntaController.getPreguntas);
api.get('/preguntasTest',md_auth.ensureAuth,PreguntaController.getPreguntasTest);
api.put('/pregunta/:id',md_auth.ensureAuth,PreguntaController.updatePregunta);
api.delete('/pregunta/:id',md_auth.ensureAuth,PreguntaController.deletePregunta);
// api.post('/upload-image-artist/:id',[md_auth.ensureAuth,md_upload],ArtistController.uploadImage);
// api.get('/get-image-artist/:imageFile',ArtistController.getImageFile);



module.exports=api;